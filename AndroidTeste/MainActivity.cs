﻿using Android.App;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using NetStandardSource.Models;
using NetStandardSource.Helpers;
using System.Linq;
using Android.Support.V4.View;
using Android.Views;
using Android.Runtime;
using System;
using Android.Content;
using AndroidTeste.Models;

namespace AndroidTeste
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        RecyclerView mRecycleView;
        RecyclerView.LayoutManager mLayoutManager;
        List<Product> products;
        ProductAdapter mAdapter;
        ImageButton imgButton;

        protected readonly ProductHelper productHelper = new ProductHelper();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.FilterMenu);
            SetContentView(Resource.Layout.Main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            

            imgButton = FindViewById<ImageButton>(Resource.Id.filterButton);
            imgButton.Click += ImgFilterButton_Click;

            

            SetProductList(0);

            
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view); 
            
            navigationView.SetNavigationItemSelectedListener(this);
        }

        public void SetProductList(int? category)
        {
            if (category == null || category == 0)
                products = productHelper.GetProducts().ToList();
            else
                products = productHelper.GetProductsByCategory(category.Value).ToList();
            
            mAdapter = new ProductAdapter(products);
            mAdapter.RemoveButtonClick += RemoveButton_Click;
            mAdapter.AddButtonClick += AddButton_Click;
            mRecycleView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mLayoutManager = new LinearLayoutManager(this);
            mRecycleView.SetLayoutManager(mLayoutManager);
            mRecycleView.SetAdapter(mAdapter);
        }

        private void MAdapter_ItemClick(object sender, int e)   
        {
            int photoNum = e + 1;
            Toast.MakeText(this, "This is item number " + photoNum, ToastLength.Short).Show();
        }

        private void ImgFilterButton_Click(object sender, EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            FilterPopup dialog = new FilterPopup();
            dialog.Show(transaction, "teste");
        }
        private void RemoveButton_Click(object sender, int e)
        {
            var textView = FindViewById<TextView>(Resource.Id.productCountTextView);
            var value = int.Parse(textView.Text);
            if(value > 0)
                value--;
            textView.Text = value.ToString();
        }

        private void AddButton_Click(object sender, int e)
        {
            var textView = FindViewById<TextView>(Resource.Id.productCountTextView);
            var value = int.Parse(textView.Text);
            value++;
            textView.Text = value.ToString();
        }
        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            //PopupMenu m = new Android.Widget.PopupMenu(this, )
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);            
            return true;
        }
       
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }
        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.nav_products)
            {
                
            }
            else if (id == Resource.Id.nav_chart)
            {

            }
            

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}

