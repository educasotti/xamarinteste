﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using NetStandardSource.Helpers;

namespace AndroidTeste.Models
{
    public class FilterPopup : DialogFragment
    {

        protected readonly CategoryHelper categoryHelper = new CategoryHelper();
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.FilterMenu, container, false);

            RadioGroup radioGroup = view.FindViewById<RadioGroup>(Resource.Id.radioGroup1);
            foreach (var item in categoryHelper.GetCategories())
            {
                var radioButton = new RadioButton(view.Context);
                radioButton.Id = item.id;
                radioButton.Text = item.name;
                radioGroup.AddView(radioButton);
            }


            var button = view.FindViewById<Button>(Resource.Id.filterFormButton);
            button.Click += delegate
            {
                new AndroidTeste.MainActivity().SetProductList(radioGroup.CheckedRadioButtonId == -1 ? 0 : radioGroup.CheckedRadioButtonId);
            };





            return view;
        }
    }
}