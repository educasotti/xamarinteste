﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidTeste.Models
{
    public class FilterPopupModel 
    {
        public RadioGroup RadioGroup { get; set; }
        public Button FilterButton { get; set; }

        public FilterPopupModel(View itemview, Action<int> FilterButtonListener)
        {
            RadioGroup = itemview.FindViewById<RadioGroup>(Resource.Id.radioGroup1);
            FilterButton = itemview.FindViewById<Button>(Resource.Id.filterButton);
        }
    }
}