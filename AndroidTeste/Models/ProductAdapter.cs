﻿using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using NetStandardSource.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AndroidTeste
{
    public class ProductAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;
        public event EventHandler<int> RemoveButtonClick;
        public event EventHandler<int> AddButtonClick;
        public IEnumerable<Product> _product;
        public ProductAdapter(IEnumerable<Product> product)
        {
            _product = product;
        }
        public override int ItemCount
        {
            get { return _product.Count(); }
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ProductHolder vh = holder as ProductHolder;
            var imageBitmap = GetImageBitmapFromUrl(_product.ElementAt(position).photo);
            vh.Image.SetImageBitmap(imageBitmap);
            vh.Name.Text = _product.ElementAt(position).name;
            vh.Description.Text = _product.ElementAt(position).description;
            vh.Price.Text = string.Format("{0:C}", _product.ElementAt(position).price);
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ProductCardView, parent, false);
            ProductHolder vh = new ProductHolder(itemView, RemoveClick, AddClick);
            return vh;
        }
        private void OnClick(int obj)
        {
            if (ItemClick != null)
                ItemClick(this, obj);
        }

        private void RemoveClick(int obj)
        {
            if (RemoveButtonClick != null)
                RemoveButtonClick(this, obj);
        }

        private void AddClick(int obj)
        {
            if (AddButtonClick != null)
                AddButtonClick(this, obj);
        }
        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

        
    }
}