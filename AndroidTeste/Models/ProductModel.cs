﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;

namespace AndroidTeste
{
    
    public class ProductHolder :  RecyclerView.ViewHolder
    {
        public ImageView Image { get; set; }
        public TextView Name { get; set; }
        public TextView Price { get; set; }
        public TextView Description { get; set; }
        public ToggleButton Fav { get; set; }
        public TextView Amount { get; set; }
        public Button RemoveButton { get; set; }
        public Button AddButton { get; set; }

        public ProductHolder(View itemview, Action<int> removeListener, Action<int> addListener) : base(itemview)
        {
            Image = itemview.FindViewById<ImageView>(Resource.Id.imageView);
            Name = itemview.FindViewById<TextView>(Resource.Id.textView);
            Price = itemview.FindViewById<TextView>(Resource.Id.priceTextView);
            Description = itemview.FindViewById<TextView>(Resource.Id.descriptionTextView);
            Fav = itemview.FindViewById<ToggleButton>(Resource.Id.favCheckButton);
            Amount = itemview.FindViewById<TextView>(Resource.Id.productCountTextView);
            RemoveButton = itemview.FindViewById<Button>(Resource.Id.removeButton);
            AddButton = itemview.FindViewById<Button>(Resource.Id.addButton);
            this.RemoveButton.Click += (sender, e) => removeListener(base.Position);
            this.AddButton.Click += (sender, e) => addListener(base.Position);
            //itemview.Click += (sender, e) => listener(base.Position);
        }
        
    }
}