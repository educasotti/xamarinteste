﻿using NetStandardSource.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetStandardSource.Helpers
{
    public class OfferHelper
    {
        private readonly DeserializeHelper _deserializeHelper = new DeserializeHelper();
        public IEnumerable<Offer> GetOffers()
        {
            var offers = new List<Offer>();
            var api = DeserializeHelper.GetFromAPI(Endpoint.Categories);
            if (api != null)
                foreach (var item in api)
                    offers.Add(JsonConvert.DeserializeObject<Offer>(item.ToString()));

            return offers;
        }
    }
}
