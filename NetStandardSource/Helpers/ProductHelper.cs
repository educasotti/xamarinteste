﻿using NetStandardSource.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetStandardSource.Helpers
{
    public class ProductHelper
    {
        private readonly DeserializeHelper _deserializeHelper = new DeserializeHelper();
        public IEnumerable<Product> GetProducts()
        {
            var products = new List<Product>();
            var api = DeserializeHelper.GetFromAPI(Endpoint.Products);
            if (api != null)
                foreach (var item in api)
                    products.Add(JsonConvert.DeserializeObject<Product>(item.ToString()));

            return products;
        }

        public IEnumerable<Product> GetProductsByCategory(int categoryId)
        {
            var products = new List<Product>();
            var api = DeserializeHelper.GetFromAPI(Endpoint.Products);
            if (api != null)
                foreach (var item in api)
                    products.Add(JsonConvert.DeserializeObject<Product>(item.ToString()));

            return products.Where(x => x.category_id == categoryId);
        }
    }
}
