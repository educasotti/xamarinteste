﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetStandardSource.Models
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}
