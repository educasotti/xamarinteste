﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetStandardSource.Models
{
    public enum Endpoint
    {
        Categories, Products, Offers
    }
}
