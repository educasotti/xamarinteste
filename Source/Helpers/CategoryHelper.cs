﻿using Source.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Source.Helpers
{
    public class CategoryHelper
    {
        private readonly DeserializeHelper _deserializeHelper = new DeserializeHelper();

        public IEnumerable<Category> GetCategories()
        {
            var categories = new List<Category>();
            var api = DeserializeHelper.GetFromAPI(Endpoint.Categories);
            if(api != null)            
                foreach(var item in api)                
                    categories.Add(JsonConvert.DeserializeObject<Category>(item.ToString()));

            return categories;          
        }
    }
}
