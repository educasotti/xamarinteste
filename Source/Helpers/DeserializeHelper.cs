﻿using Source.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Source.Helpers
{
    public class DeserializeHelper
    {
        public static dynamic GetFromAPI(Endpoint endpoint)
        {
            try
            {
                var value = string.Empty;
                using (WebClient webClient = new WebClient())
                {
                    var webAddr = "https://pastebin.com/raw/";
                    webClient.Encoding = Encoding.UTF8;
                    value = webClient.DownloadString(webAddr + GetEndpoint(endpoint));
                }
                return JsonConvert.DeserializeObject(value);

            }
            catch(WebException e)
            {
                throw e;
            }
        }

        public static string GetEndpoint(Endpoint endpoint)
        {
            switch(endpoint)
            {
                case Endpoint.Categories:
                    return "YNR2rsWe";
                case Endpoint.Products:
                    return "eVqp7pfX";
                case Endpoint.Offers:
                    return "R9cJFBtG";
                default:
                    return null;
            }
        }
    }

    
}
