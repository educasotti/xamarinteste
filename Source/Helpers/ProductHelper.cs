﻿using Newtonsoft.Json;
using Source.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Source.Helpers
{
    public class ProductHelper
    {
        private readonly DeserializeHelper _deserializeHelper = new DeserializeHelper();
        public IEnumerable<Product> GetProducts()
        {
            var products = new List<Product>();
            var api = DeserializeHelper.GetFromAPI(Endpoint.Categories);
            if (api != null)
                foreach (var item in api)
                    products.Add(JsonConvert.DeserializeObject<Product>(item.ToString()));

            return products;
        }
    }
}
