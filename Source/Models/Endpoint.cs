﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Source.Models
{
    public enum Endpoint
    {
        Categories, Products, Offers
    }
}
