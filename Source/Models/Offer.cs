﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Source.Models
{

    public class Offer
    {
        public string name { get; set; }
        public int category_id { get; set; }
        public IEnumerable<Policy> policies { get; set; }
    }

    public class Policy
    {
        public int min { get; set; }
        public float discount { get; set; }
    }

}
