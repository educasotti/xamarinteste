﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Source.Models
{

    public class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string photo { get; set; }
        public float price { get; set; }
        public int category_id { get; set; }
        public virtual Category Category { get; set; }
    }

}
