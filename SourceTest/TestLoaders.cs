using NUnit.Framework;
using NetStandardSource.Helpers;
using System.Linq;

namespace Tests
{
    public class Tests
    {
        private readonly CategoryHelper _categoryHelper = new CategoryHelper();
        private readonly ProductHelper _productHelper = new ProductHelper();
        private readonly OfferHelper _offerHelper = new OfferHelper();
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void LoadCategories_ShouldContainAtLeast1()
        {
            var categories = _categoryHelper.GetCategories();
            Assert.Greater(categories.Count(), 0);
        }

        [Test]
        public void LoadProducts_ShouldContainAtLeast1()
        {
            var products = _productHelper.GetProducts();
            Assert.Greater(products.Count(), 0);
        }

        [Test]
        public void LoadProductsByCategory_ShouldContainAtLeast1()
        {
            var products = _productHelper.GetProductsByCategory(1);
            Assert.Greater(products.Count(), 0);
        }

        [Test]
        public void LoadOffers_ShouldContainAtLeast1()
        {
            var offers = _offerHelper.GetOffers();
            Assert.Greater(offers.Count(), 0);
        }
    }
}